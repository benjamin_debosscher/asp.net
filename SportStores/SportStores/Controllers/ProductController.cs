﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using SportStores.Models;
using System.Linq;
namespace SportStores.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository repository;
        public int pageSize = 4;
        public ProductController(IProductRepository repo)
        {
            repository = repo;
        }

        public ViewResult List(int productPage =1)             => View(repository.Products.OrderBy(p=>p.ProductID).Skip((productPage-1)*pageSize).Take(pageSize));
    }

}
